import * as Joi from '@hapi/joi';

const BrandValidationSchema = Joi.object().keys({
  name: Joi.string().required().min(3),
  isActive: Joi.boolean(),
});

export default BrandValidationSchema;
