import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

@Injectable()
export class DataService {
  constructor(private readonly model: Model<any>, private readonly entity: any) {
  }

  async findAll(): Promise<any> {
    return this.model.find().select(this.entity);
  }

  async getById(id): Promise<any> {
    return await this.model.find({ _id: id });
  }

  create(resource): Promise<any> {
    const createdResource = new this.model(resource);
    return createdResource.save();
  }

  async delete(id: number): Promise<any> {
    const deletedBrand = this.model.deleteOne({ _id: id });
    return await deletedBrand;
  }

  async update(id: number, resource): Promise<any> {
    const updatedBrand = this.model.updateOne({ _id: id }, resource);
    return await updatedBrand;
  }
}
