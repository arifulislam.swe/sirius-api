import { Injectable } from '@nestjs/common';
import { DataService } from '../../shared/services/data/data.service';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class UsersService extends DataService {
  constructor(@InjectModel('User') private readonly userModel: Model<any>) {
    super(userModel, {});
  }

  async findUserByEmail(email: string) {
    return await this.userModel.findOne({email});
  }
}
