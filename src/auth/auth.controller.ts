import { Body, Controller, Post, Request } from '@nestjs/common';
import { AuthService } from './service/auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @Post('login')
  async login(@Body() body) {
    return this.authService.login(body);
  }
}
