import { Module } from '@nestjs/common';
import { BrandsController } from './brands/brands.controller';
import { BrandsService } from './brands/brands.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BrandSchema } from './schemas/brand.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Brand', schema: BrandSchema }])],
  controllers: [BrandsController],
  providers: [BrandsService],
})
export class BrandsModule {
}
