import * as mongoose from 'mongoose';

export const BrandSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  isActive: Boolean,
});
