import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../../users/service/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private jwtService: JwtService,
  ) {
  }

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.findUserByEmail(email);

    if (user && user.password === pass) {
      return user;
    }

    throw new UnauthorizedException('Username or password is incorrect');
  }

  async login(user: any) {
    const result = await this.validateUser(user.email, user.password);

    if (result) {
      const payload = { email: result.email, sub: result._id };

      return {
        access_token: this.jwtService.sign(payload),
      };
    }
  }
}

export const jwtConstants = {
  secret: 'secretKey',
};
