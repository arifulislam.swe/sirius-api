import { Body, Delete, Get, Param, Post, Put, UseGuards, UsePipes } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JoiValidationPipe } from '../pipes/joiValidation.pipe';
import BrandValidationSchema from '../../brands/validators/brand.validator';

export class BaseController {
  constructor(private readonly dataService: any, private entity: any) {
  }

  @Get('/')
  @UseGuards(AuthGuard('jwt'))
  getAll(): any {
    return this.dataService.findAll();
  }

  @Get('/:id')
  getById(@Param() params: any): any {
    return this.dataService.getById(params.id);
  }

  @Post('/')
  @UsePipes(new JoiValidationPipe(BrandValidationSchema))
  create(@Body() body): any {
    return this.dataService.create(body);
  }

  @Delete('/:id')
  delete(@Param() params: any): any {
    return this.dataService.delete(params.id);
  }

  @Put('/:id')
  update(@Param() params: any, @Body() body: any) {
    return this.dataService.update(params.id, body);
  }
}
