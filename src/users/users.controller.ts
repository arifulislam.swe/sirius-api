import { Body, Controller, Post } from '@nestjs/common';
import { UsersService } from './service/users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {
  }

  @Post('')
  create(@Body() body): Promise<any> {
    return this.usersService.create(body);
  }
}
