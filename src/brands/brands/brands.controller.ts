import { Controller } from '@nestjs/common';
import { BrandsService } from './brands.service';
import { BaseController } from '../../shared/controllers/base.controller';
import { BrandEntity } from '../entities/brand.entity';

@Controller('brands')
export class BrandsController extends BaseController {

  constructor(
    private readonly brandsService: BrandsService,
  ) {
    super(brandsService, BrandEntity);
  }
}
