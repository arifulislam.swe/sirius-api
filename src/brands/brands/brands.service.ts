import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { DataService } from '../../shared/services/data/data.service';
import { BrandEntity } from '../entities/brand.entity';

@Injectable()
export class BrandsService extends DataService {
  constructor(@InjectModel('Brand') private readonly brandModel: Model<any>) {
    super(brandModel, BrandEntity);
  }
}
